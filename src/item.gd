extends Button

@export var cmd = "quit"

var tween : Tween

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass


func _on_button_down() -> void:
	if cmd == "quit":
		get_tree().quit()
		
	var output = []
	DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
	var exit_code = OS.execute(cmd, output)
	#Global.hud += exit_code
	print(exit_code)
	for o in output:
		Global.hud += o
		print(o)
	
	if cmd == "retroarch":
		get_tree().quit()
	
	get_tree().paused = true
	DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
#	for o in output:
#		print("output")
#		print(exit_code)
	

func _on_focus_entered() -> void:
	get_tree().paused = false
	Global.scoll_position = position.y
	tween = create_tween() # Creates a new tween
	tween.tween_property(self, "scale", Vector2(1.2,1.2), .5)

func _on_focus_exited() -> void:
	tween = create_tween() # Creates a new tween
	tween.tween_property(self, "scale", Vector2(1,1), .5)
