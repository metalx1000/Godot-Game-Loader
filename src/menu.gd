extends Control

var home_path = OS.get_environment("HOME")
var game_path = home_path + "/roms/godot"

@onready var menu_list = $ScrollContainer/List
@onready var entries =  preload("res://item.tscn")
@onready var scrollbox = $ScrollContainer
var tween : Tween

var scroll_pos = 0
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	#load_list()
	dir_contents(game_path)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	#if scroll_pos != Global.scoll_position:
	tween = create_tween() # Creates a new tween
	tween.tween_property(scrollbox, "scroll_vertical", Global.scoll_position - 10, .5)
	$Label.text = Global.hud

func dir_contents(path):
	var dir = DirAccess.open(path)
	if dir:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if file_name.contains(".x86_64"):
				print("Game Found: " + file_name)
				load_entry(file_name)
			file_name = dir.get_next()
	else:
		print("An error occurred when trying to access the path.")

func load_entry(file):
	var entry = entries.instantiate()
	var title = file.replace("-", " ")
	title = file.replace("_", " ")
	title = title.split(".")[0]
	
	entry.add_to_group("games")
	entry.cmd = game_path + "/" + file 
	entry.text = title
	menu_list.add_child(entry)
	
func _notification(what):
	if what == MainLoop.NOTIFICATION_APPLICATION_FOCUS_IN:
		get_tree().paused = false
	elif what == MainLoop.NOTIFICATION_APPLICATION_FOCUS_OUT:
		print("pause")
		get_tree().paused = true

func _on_timer_timeout() -> void:
	get_tree().get_first_node_in_group("games").grab_focus()
	var first_game = get_tree().get_first_node_in_group("games")
	first_game.position.y
	scrollbox.set_deferred("scroll_vertical", first_game.position.y)
