extends Node3D

@onready var spinner = $spinner_01

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	load_model()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	spinner.rotate_y(delta)
	pass

func load_model():
	var models = get_tree().get_nodes_in_group("models")
	for model in models:
		model.set_visible(false)
	randomize()
	models.shuffle()
	models[0].visible = true
