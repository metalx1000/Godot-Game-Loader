extends Node3D

@onready var box = $box
@onready var skys = [
	"res://objects/skybox/sky.jpg",
	"res://objects/skybox/sunset_01.png",
	"res://objects/skybox/sky_02.png",
	"res://objects/skybox/sunset_02.png",
	"res://objects/skybox/sunset_03.jpg"
]

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	load_sky()
	pass
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	rotate_y(delta*-.05)
	pass

func load_sky():
	randomize()
	skys.shuffle()
	print(skys[0])
	var img = load(skys[0])
	
	box.get_surface_override_material(0).albedo_texture = img
