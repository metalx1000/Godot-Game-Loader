#!/bin/bash
######################################################################
#Copyright (C) 2024  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
######################################################################

dir="$HOME/roms/godot"
mkdir -p "$dir"

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
echo $SCRIPT_DIR

function version_check() {
	project="$1"
	if [[ $(grep "config_version=5" "$project") ]]; then
		echo -n "godot4"
	else
		echo -n "godot3"
	fi
}

find $dir -name "*.x86_64" | sort | while read exe; do
	title="$(basename $exe | sed 's/.x86_64//g' | tr "-" " " | tr "_" " ")"
	echo "$title|$exe"
done | tee "$SCRIPT_DIR/game_list.txt"
